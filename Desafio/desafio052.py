n = int(input('Digite um número inteiro: '))
total = 0
for c in range (1, n+1):
    if n % c == 0:
        print('\033[32m', end='')
        total += 1
    else:
        print('\033[31m', end='')
    print(f'{c} ', end='')
print(f'\n\033[m O número {n} foi divisível {total} vezes')
if total == 2:
    print(f'Portanto ele é um número primo')
else:
    print(f' Portanto ele NÃO é um número primo')