funcao = 4
while funcao != 5:
    if funcao == 1:
        resultado = v1 + v2;
        print(f'A soma de {v1} e {v2} é {resultado}')
    if funcao == 2:
        resultado = v1 * v2;
        print(f'A multiplicação de {v1} e {v2} é {resultado}')
    if funcao == 3:
        if v1 > v2:
            print(f'O maior número digitado foi: {v1}')
        elif v1 < v2:
            print(f'O maior número digitado foi: {v2}')
        else:
            print('Os números digitados são iguais, portanto não existe um maior')
    if funcao == 4:
        v1 = int(input('Digite o primeiro número: '))
        v2 = int(input('Digite o segundo número: '))
    print('\n \033[32m Digite o número da função desejada: \n')
    print('\033[30m [1] Somar')
    print(' [2] Multiplicar')
    print(' [3] Maior')
    print(' [4] Novos Números')
    print(' [5] Sair do Programa')
    funcao = int(input(' FUNÇÃO: \n'))
print('FIM')