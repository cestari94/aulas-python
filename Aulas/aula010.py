nome = str(input('Digite seu nome: ')).strip()
if nome.capitalize() == 'Eduardo':
    print('Bom dia, Eduardo! Seu nome é muito bonito!')
else:
    print(f'Bom dia, {nome}')
