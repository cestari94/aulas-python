print('=-'*30)
print('{:^50}'.format('INSIRA OS ITENS DO PEDIDO'))
print('=-'*30)
c = s = m1k = 0
bpreco = 999999
mais = 'a'
while mais != 'N':
    nome = str(input('Nome do produto: '))
    preco = float(input('Preço: R$ '))
    if preco < bpreco:
        bnome = nome
        bpreco = preco
    if preco > 1000:
        m1k += 1
    s += preco
    c += 1
    print('=-'*20)
    mais = 'a'
    while mais not in 'SN':
        mais = str(input('Deseja inserir outro produto: [S/N]')).upper().strip()[0]
    print('=-'*20)
print('=-'*30)
print('{:^50}'.format('FIM DO PEDIDO'))
print('=-'*30)
print(f'Total de Itens: \033[32m{c}\033[30m - Total a pagar: \033[32mR${s:.2f}\033[30m' )
print(f'O produto mais barato foi: \033[32m{bnome.upper()}\033[30m e custou \033[32m{bpreco:.2f}\033[30m')
print(f'Ao todo nesta compra foram inseridos \033[32m{m1k}\033[30m produtos acima de R$ 1000,00')
