from random import randint
j1 = v = 0
j2 = 'a'
while True:
    print('\033[32m\nVamos jogar PAR ou ÍMPAR \033[30m')
    j1 = int(input('Digite um valor'))
    pc1 = randint(0, 10)
    t = (j1 + pc1)
    while j2 not in 'PpIi':
        j2 = str(input('PAR ou ÍMPAR? [P/I]')).upper().strip()[0]
    print(f'\nVocê jogou {j1} e o computador jogou {pc1}. Total foi {t}')
    if ((t % 2 == 0) and (j2 == 'P')) or ((t % 2 == 1) and (j2 == 'I')):
        print('Parabéns, você venceu!')
        v += 1
        j2 = 'a'
    else:
        print(f'Não foi desta vez, você venceu {v} vezes.')
        break
print('FIM')