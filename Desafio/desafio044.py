produto = float(input('Digite o valor do produto: '))
pag = int(input('Escola a forma de pagamento: \n 1 - À vista no dinheiro ou cheque \n 2 - À vista no cartão \n 3 - Até 2x sem juros no cartão \n 4 - 3x ou mais no cartão com 20% de juros \n' ))
if pag == 1:
    preco = produto - produto*(10/100)

elif pag == 2:
    preco = produto - produto*(5/100)
elif pag == 4:
    preco = produto + produto*(20/100)
    parcelas = int(input('Digite a quantidade de parcelas: '))
    print(f'Sua compra será parcelada em {parcelas}x de R${produto/parcelas:.2f} com juros')
elif pag == 3:
    preco = produto
else:
    preco = produto
    print('OPÇÃO DE PAGAMENTO INVÁLIDA, TENTE NOVAMENTE!')
print(f'O valor total a pagar é R${preco:.2f}')