n1 = float(input('Digite a primeira nota: '))
n2 = float(input('Digite a segunda nota: '))
media = (n1 + n2) / 2
if media >= 6:
    print(f'Parabéns, você foi aprovado com média: {media}')
else:
    print(f'Ah, não foi dessa vez, você teve média: {media}, espero que na próxima se saia melhor!')
