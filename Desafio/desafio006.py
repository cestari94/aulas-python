n = int(input('Digite um número: '))
dobro = n * 2
triplo = n * 3
raiz = n ** 0.5
print(f'O número digitado foi: {n}, o dobro dele é: {dobro}, seu triplo é: {triplo} e sua raiz quadrada é: {raiz}')