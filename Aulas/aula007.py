n1 = int(input('Digite um número: '))
n2 = int(input('Digite outro número: '))
soma = n1 + n2
subtracao = n1 - n2
divisao = n1 / n2
multiplicacao = n1 * n2
di = n1 // n2
e = n1 ** n2
print(' A soma é: ', soma)
print(f' A subtração é: {subtracao}')
print(' A divisão é: {:.2f}'.format(divisao), end=' || ')
print(f' A multiplicação é: {multiplicacao}')
print(f' A divisão inteira é: {di} \n A potência é: {e}')