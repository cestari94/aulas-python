n = int(input('Digite um número para descobrir seu fatorial: '))
sub = n
fatorial = n
while sub > 0:
    if (sub != n):
        fatorial *= sub
    sub -= 1
print(f'O fatorial de \033[36m{n} \33[30mé \033[32m{fatorial}')