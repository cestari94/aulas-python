casa = float(input('Digite o valor da casa: '))
s = float(input('Digite o valor do seu salário: '))
anos = float(input('Digite em quantos anos pretende pagar: '))
prestacao = casa / (anos * 12)
limitep = s * 30/100
print(f'Nesta simulação as prestações ficam em {anos*12:.0f}x de R${prestacao:.2f}')
if prestacao > limitep:
    print('Desculpe, você não pode comprar esta casa.')
else:
    print('Com este valor você poderá comprar esta casa!')