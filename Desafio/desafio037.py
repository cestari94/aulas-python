n = int(input('Digite um número inteiro: '))
bc = int(input('Escolha a base de conversão: \n [ 1 ] Binário \n [ 2 ] Octal \n [ 3 ] Hexadecimal \n Selecione: \n'))
if bc == 1:
    print(f'{n} convertido para Binário é: {bin(n)[2:]}')
elif bc == 2:
    print(f'{n} convertido para Octal é: {oct(n)[2:]}')
elif bc == 3:
    print(f'{n} convertido para Hexadecimal é: {hex(n)[2:]}')
else:
    print('Você fez algo de errado, aperte F5 e tente novamente!')
