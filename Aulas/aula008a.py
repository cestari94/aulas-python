from math import sqrt
num = int(input('Digite um número: '))
raiz = sqrt(num)
print(f'A raiz quadrada de {num} é: {raiz:.2f}')
##print('A raiz quadrada de {} é {:.2f}'.format(num, raiz))