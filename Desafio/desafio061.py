t = int(input('Digite o primeiro termo da progressão aritmética: '))
r = int(input('Digite a razão da progressão aritmética: '))
c = 0
while c < 10:
    print(t, end=' ➳ ')
    c += 1
    t += r
print('FIM')
