n = int(input('Digite um número para exibir a sequência de Fribonacci até ele: '))
c = 0
fa = 0
fd = 1
while c < n:
    print(f'{fd} ', end=' ')
    fd += fa
    fa = fd - fa
    c += 1
print('FIM')