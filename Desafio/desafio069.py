maior = h = fm20 = 0
new = 'S'
while new == 'S':
    while sexo not in 'MmFf':
        sexo = str(input('Digite o sexo: [M/F] ')).upper().strip()[0]
    idade = int(input('Digite a idade: '))
    if idade > 18:
        maior += 1
    if sexo == 'M':
        h += 1
    if sexo == 'F' and idade < 20:
        fm20 += 1
    while new not in 'SsNn':
        new = str(input('Deseja continuar? [S/N]')).upper().strip()[0]
print(f'Foram cadastrados {maior} pessoas maiores de 18 anos.')
print(f'Foram cadastrados no total {h} homens')
print(f'Foram cadastradas no total {fm20} mulheres menores de 20 anos')