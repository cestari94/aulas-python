c = 1
total = soma = maior = 0
menor = 999
while c != 0:
    n = int(input('Digite um número inteiro: '))
    soma += n
    total += 1
    if n > maior:
        maior = n
    if n < menor:
        menor = n
    print('\033[33m Deseja digitar outro número? \033[30m \n [1] SIM \n [0] NÃO')
    c = int(input('Resposta \n'))
print(f'Você digitou {total} números, a soma do valor deles é {soma} e a média deles é {int(soma/total)}')
print(f'O menor número digitado foi: {menor}')
print(f'O maior número digitado foi: {maior}')

