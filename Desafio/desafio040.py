n1 = float(input('Digite a primeira nota: '))
n2 = float(input('Digite a segunda nota: '))
media = (n1+n2) / 2
if media < 5:
    print(f'Você teve {media} de média e foi reprovado!')
#elif media >= 5 and media <= 6.9:
elif 6.9 > media >= 5:
    print(f'Você teve {media} de média e terá que ficar de recuperação')
else:
    print(f'Você teve {media} de média e foi aprovado. Parabéns!')