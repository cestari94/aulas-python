from datetime import date
anasc = int(input('digite o ano de seu nascimento: '))
if (anasc + 18) == date.today().year:
    print(f'Parabéns, neste ano você completa 18 anos e deverá se alistar!')
elif (anasc + 18) < date.today().year:
    saldo = date.today().year - (anasc + 18)
    print(f'Já passou da hora de se alistar, você está atrasado há {saldo} ano(s)')
else:
    saldo = (anasc + 18) - date.today().year
    print(f'Ainda não chegou sua hora, você deve se alistar apenas daqui há {saldo} anos')