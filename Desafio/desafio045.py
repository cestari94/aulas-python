from random import randint
print('Vamos jogar Jokenpô!')
mao = int(input('Escolha uma opção: \n 1 - Pedra \n 2 - Papel \n 3 - Tesoura \n \n'))
ia = randint(1,3)
if ia == mao:
    print('Empatamos! Aperte F5 e vamos jogar novamente!')
elif ia == 1:
    print('Eu escolhi Pedra!')
    if mao == 2:
        print('ôh não, papel enrola pedra, você venceu!')
    else:
        print('Yeaaah, pedra quebra tesoura, eu venci!')
elif ia == 2:
    print('Eu escolhi Papel!')
    if mao == 3:
        print('ôh não, tesoura corta papel, você venceu!')
    else:
        print('Yeaaah, papel enrola pedra, eu venci!')
else:
    print('Eu escolhi Tesoura!')
    if mao == 1:
        print('ôh não, pedra quebra tesoura, você venceu!')
    else:
        print('Yeaaah, tesoura corta papel, eu venci!')
print('\nAperte F5 e vamos jogar novamente!')