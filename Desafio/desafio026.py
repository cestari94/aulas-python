frase = str(input('Digite uma frase: ')).strip()
print(f'A letra "A" está presente {frase.lower().count("a")} vezes em sua frase')
print(f'A letra "A" aparece pela primeira vez em sua frase na posição {frase.lower().find("a")+1}')
print(f'A letra "A" aparece pela ultima vez em sua frase na posição {frase.lower().rfind("a")+1}')
