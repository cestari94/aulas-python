peso = float(input('Digite seu peso atual: '))
alt = float(input('Digite sua altura: '))
imc = peso / (alt ** 2)
if imc < 18.5:
    print(f'Seu IMC é: {imc:.1f} e você está Abaixo do Peso')
elif imc < 25:
    print(f'Seu IMC é: {imc:.1f} e você está no Peso Ideal')
elif imc < 30:
    print(f'Seu IMC é: {imc:.1f} e você está com Sobrepeso')
elif imc < 40:
    print(f'Seu IMC é: {imc:.1f} e você está com Obesidade')
else:
    print(f'Seu IMC é: {imc:.1f} e você está com Obesidade Mórbida')