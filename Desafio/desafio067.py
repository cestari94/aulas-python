while True:
    n = int(input('Digite um número para ver sua tabuada: '))
    print('-'*40)
    if n < 0:
        break
    c = 1
    for c in range (1,11):
        print(f'{c} x {n} = {c*n}')
    print('-'*40)
print('\033[35mTabuada Encerrada. Volte sempre!')
