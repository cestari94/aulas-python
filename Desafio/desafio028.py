from random import randint
n = int(input('Digite um número de 0 a 5: '))
npc = randint(0,5)
if n == npc:
    print('Você acertou o número sorteado! Parabéns!')
else:
    print(f'Não foi desta vez, você disse {n}, mas eu pensei em {npc}, aperte F5 e jogue novamente!')
