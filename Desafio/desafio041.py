from datetime import date
nome = str(input('Digite o nome do atleta: '))
anasc = int(input('Digite o ano de nascimento do atleta: '))
idade = date.today().year - anasc
if idade <= 9:
    print(f'O atleta {nome} possui {idade} anos e está classificado na categoria MIRIM')
elif idade <= 14:
    print(f'O atleta {nome} possui {idade} anos e está classificado na categoria INFANTIL')
elif idade <= 19:
    print(f'O atleta {nome} possui {idade} anos e está classificado na categoria JUNIOR')
elif idade <= 20:
    print(f'O atleta {nome} possui {idade} anos e está classificado na categoria SENIOR')
else:
    print(f'O atleta {nome} possui {idade} anos e está classificado na categoria MASTER')