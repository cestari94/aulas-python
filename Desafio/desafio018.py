import math
a = int(input('Digite o valor do ângulo: '))
print(f' O valor do seno desse ângulo é: {math.sin(a):.2f} \n O valor do cosseno desse ângulo é: {math.cos(a):.2f} \n E o valor da tangente desse ângulo é: {math.tan(a):.2f} ')